package dao;

import model.Author;
import model.Book;
import model.Genre;

public interface IBookDao<T extends Book> extends IBaseDao<T> {
    boolean isIdPresent(Integer id, String idName, String tableName);
    void addAuthor(Author author);
    void addGenre(Genre genre);
}
