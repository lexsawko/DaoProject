package dao.Impl;

import dao.IBookDao;
import model.Author;
import model.Book;
import model.Genre;
import util.ConnectionPool;
import util.PreparedStatementSupplier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements IBookDao<Book> {
    private Connection conn;
    private PreparedStatementSupplier psSupplier = new PreparedStatementSupplier();

    private void getConnection(){
        conn = ConnectionPool.getInstance().getConnecton();
        psSupplier.setConnection(conn);
    }

    @Override
    public List<Book> findAll() {
        getConnection();
        final String GET_ALL_BOOKS = "SELECT * FROM books INNER JOIN author ON books.author_id=author.idauthor " +
                "INNER JOIN genre ON books.genre_id=genre.idgenre";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL_BOOKS);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt("book_id"));
                book.setName(resultSet.getString("book_name"));
                book.setPages(resultSet.getInt("pages"));
                book.setAge(resultSet.getInt("age"));
                book.setGenre(new Genre(resultSet.getInt("idgenre"), resultSet.getString("genre_name")));
                book.setAuthor(new Author(resultSet.getInt("idauthor"),
                        resultSet.getString("first_name"), resultSet.getString("last_name")));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().returnConnection(conn);
        }
        return books;
    }

    @Override
    public Book findEntityById(Integer id) {
        getConnection();
        Book book = new Book();
        try (PreparedStatement preparedStatement = psSupplier.getBookByIdPS(id);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                book.setId(resultSet.getInt("book_id"));
                book.setName(resultSet.getString("book_name"));
                book.setPages(resultSet.getInt("pages"));
                book.setAge(resultSet.getInt("age"));
                book.setGenre(new Genre(resultSet.getInt("idgenre"), resultSet.getString("genre_name")));
                book.setAuthor(new Author(resultSet.getInt("idauthor"),
                        resultSet.getString("first_name"), resultSet.getString("last_name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().returnConnection(conn);
        }
        return book;
    }

    @Override
    public boolean delete(Book book) {
        getConnection();
        try (PreparedStatement preparedStatement = psSupplier.deleteBookPS(book)) {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().returnConnection(conn);
        }
        return false;
    }

    @Override
    public boolean update(Book book) {
        getConnection();
        try (PreparedStatement preparedStatement = psSupplier.updateBookPS(book)) {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().returnConnection(conn);
        }
        return false;
    }

    @Override
    public boolean create(Book book) {
        getConnection();
        try (PreparedStatement preparedStatement = psSupplier.createBookPS(book)) {
            conn.setAutoCommit(false);
            boolean isAuthorPresent = isIdPresent(book.getAuthor().getId(), "idauthor", "author");
            boolean isGenrePresent = isIdPresent(book.getGenre().getId(), "idgenre", "genre");
            if (!isAuthorPresent) {
                addAuthor(book.getAuthor());
            }
            if (!isGenrePresent) {
                addGenre(book.getGenre());
            }
            preparedStatement.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            System.err.println("Transaction was declined");
            e.printStackTrace();
        } finally {
            try {
                conn.setAutoCommit(true);
                ConnectionPool.getInstance().returnConnection(conn);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean isIdPresent(Integer id, String idName, String tableName) {
        getConnection();
        final String GET_ID_LIST = "SELECT " + idName + " FROM " + tableName;
        List<Integer> idList = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_ID_LIST);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                idList.add(resultSet.getInt(idName));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().returnConnection(conn);
        }
        return idList.stream().anyMatch(x -> x.equals(id));
    }

    public void addAuthor(Author author) {
        getConnection();
        try (PreparedStatement preparedStatement = psSupplier.addAuthor(author)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().returnConnection(conn);
        }
    }

    public void addGenre(Genre genre) {
        getConnection();
        try (PreparedStatement preparedStatement = psSupplier.addGenre(genre)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().returnConnection(conn);
        }
    }


    public PreparedStatementSupplier getPsSupplier() {
        return psSupplier;
    }

    public void setPsSupplier(PreparedStatementSupplier psSupplier) {
        this.psSupplier = psSupplier;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public Connection getConn() {
        return conn;
    }
}
