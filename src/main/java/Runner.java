import dao.Impl.BookDaoImpl;
import model.Author;
import model.Book;
import model.Genre;
import util.ConnectionPool;
import util.PoolConfig;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        PoolConfig poolConfig = new PoolConfig();
        poolConfig.setDbName("library");
        poolConfig.setHost("localhost");
        poolConfig.setPort(3306);
        poolConfig.setIdleTimeLimit(3000);
        poolConfig.setIncrementPortion(4);
        poolConfig.setLoadFactor(0.8F);
        poolConfig.setMinPoolSize(10);
        poolConfig.setMaxPoolSize(20);
        ConnectionPool.getInstance().configuratePool(poolConfig);

        BookDaoImpl bookDAO = new BookDaoImpl();
        Book book = new Book();
        book.setId(4);
        book.setAge(1997);
        book.setAuthor(new Author(4, "Alex", "Sawko"));
        book.setGenre(new Genre(1, "Fantasy"));
        book.setName("Name");
        book.setPages(1488);
        bookDAO.create(book);

        List<Book> books = bookDAO.findAll();
        books.forEach(System.out::println);

        book.setName("The best book");
        bookDAO.update(book);

        System.out.println(bookDAO.findEntityById(book.getId()));
        bookDAO.delete(book);
    }
}
