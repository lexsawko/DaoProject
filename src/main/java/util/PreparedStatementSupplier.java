package util;

import model.Author;
import model.Book;
import model.Genre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PreparedStatementSupplier {
    private Connection connection;

    public PreparedStatement getBookByIdPS(int bookId) throws SQLException {
        final String GET_BOOK_BY_ID = "SELECT * FROM books INNER JOIN author ON books.author_id=author.idauthor " +
                "INNER JOIN genre ON books.genre_id=genre.idgenre WHERE books.book_id=?";
        PreparedStatement ps = connection.prepareStatement(GET_BOOK_BY_ID);
        ps.setInt(1, bookId);
        return ps;
    }

    public PreparedStatement createBookPS(Book book) throws SQLException {
        final String INSERT_INTO_BOOKS = "INSERT INTO books (book_id, book_name, pages, age, genre_id, author_id)" +
                "VALUES (?, ?, ?, ?, ?,?);";
        PreparedStatement ps = connection.prepareStatement(INSERT_INTO_BOOKS);
        ps.setInt(1, book.getId());
        ps.setString(2, book.getName());
        ps.setInt(3, book.getPages());
        ps.setInt(4, book.getAge());
        ps.setInt(5, book.getGenre().getId());
        ps.setInt(6, book.getAuthor().getId());
        return ps;
    }

    public PreparedStatement updateBookPS(Book book) throws SQLException {
        final String UPDATE_BOOK = "UPDATE books SET book_name=?, pages=?, age=?, genre_id=?, author_id=? " +
                "WHERE book_id=?;";
        PreparedStatement ps = connection.prepareStatement(UPDATE_BOOK);
        ps.setString(1, book.getName());
        ps.setInt(2, book.getPages());
        ps.setInt(3, book.getAge());
        ps.setInt(4, book.getGenre().getId());
        ps.setInt(5, book.getAuthor().getId());
        ps.setInt(6, book.getId());
        return ps;
    }

    public PreparedStatement deleteBookPS(Book book) throws SQLException {
        final String DELETE_BOOK = "DELETE FROM books WHERE book_id=? AND book_name=? AND " +
                "pages=? AND age=? AND genre_id=? AND author_id=?;";
        PreparedStatement ps = connection.prepareStatement(DELETE_BOOK);
        ps.setInt(1, book.getId());
        ps.setString(2, book.getName());
        ps.setInt(3, book.getPages());
        ps.setInt(4, book.getAge());
        ps.setInt(5, book.getGenre().getId());
        ps.setInt(6, book.getAuthor().getId());
        return ps;
    }

    public PreparedStatement addAuthor(Author author) throws SQLException {
        final String ADD_AUTHOR = "INSERT INTO author (idauthor, first_name, last_name) " +
                "VALUES (?, ?, ?);";
        PreparedStatement ps = connection.prepareStatement(ADD_AUTHOR);
        ps.setInt(1, author.getId());
        ps.setString(2, author.getFirstName());
        ps.setString(3, author.getLastName());
        return ps;
    }

    public PreparedStatement addGenre(Genre genre) throws SQLException {
        final String ADD_GENRE = "INSERT INTO genre (idgenre, genre_name) " +
                "VALUES (?, ?);";
        PreparedStatement ps = connection.prepareStatement(ADD_GENRE);
        ps.setInt(1, genre.getId());
        ps.setString(2, genre.getName());
        return ps;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
